Ceci est un tutoriel à diffuser. Il est amené à évoluer en fonction des mises à jour de Framadate.

Téléchargez simplement l'ensemble des fichiers et ouvrez tutoframadate.html avec votre navigateur.

Voici une version markdown (mise en page basique)


--------

# Planifier rapidement un rendez-vous avec Framadate

_Un service libre respectueux des données privées, fourni par [Framasoft](http://framasoft.org)_

## 0. Ouverture

![illustration](img/1.png)

Pour créer un nouveau sondage, rendez-vous sur [Framadate.org](http://framadate.org)
			
Puis cliquez sur « Créer un sondage spécial dates »

_Notez que vous pourrez retrouver tous vos sondages en cliquant sur « Où sont mes sondages ». Il suffira alors de renseigner votre adresse courriel._


## 1.  Configuration

![Illustration](img/2.png)
		


Remplissez le formulaire de présentation du sondage. 	
			

Votre adresse courriel ne sera utilisée que pour communiquer les informations relatives au sondage (conformément à [la charte des services Framasoft](http://degooglisons-internet.org/nav/html/charte.html)

__Cases à cocher :__ choisissez parmi les options proposées. Astuce : recevoir un  courriel à chaque fois qu'un sondé remplit une ligne est utile pour  suivre l'avancée du sondage.


![Illustration](img/3.png)

Définissez les dates du sondage en utilisant le menu contextuel affichant un calendrier, puis affinez vos dates en entrant des horaires. Les boutons + et – vous permettent d'ajouter autant de champs que nécessaire.

Notez le bouton suivant qui, une fois définis les horaires pour une première date, vous permet de cloner ces horaires, si besoin.
			
![Illustration](img/4.png)




Dans le récapitulatif de votre sondage, vous pouvez préciser vous-même une date de suppression automatique du sondage.

Vous pouvez alors __valider__ la création de votre sondage

![Illustration](img/5.png)




## 2. Administration


![Illustration](img/6.png)

Après la création du sondage, vous accédez directement à son interface d'administration. Ces données vous sont envoyées par courriel avec 2 adresses :

   * __le lien public du sondage__ à faire parvenir à vos correspondants,
   * __le lien d'administration__ à conserver pendant la durée du sondage.

Si besoin, vous pouvez d’ores et déjà accomplir votre propre participation au sondage ou indiquer vous-même la participation d'une personne.

Notez que depuis l'interface d'administration vous pourrez __exporter__ les résultats du sondage en .csv (à ouvrir avec un tableur comme Excel ou LibreOffice Calc). Pour cela, cliquez sur le bouton « Export en CSV ».



## 3. Procédure de votation

![Illustration](img/7.png)

__a.__ Indiquer son nom.

__b.__ Pour chaque champ, choisir « oui », « si nécessaire » ou « non » (par défaut).

__c.__ Enregistrer ses choix.

__d.__ Il est possible de visualiser les résultats du sondage sous forme graphique.

__e.__ À chaque vote validé, les résultats du sondage sont actualisés.



Auteur : Christophe Masutti, Framasoft, juin 2015

Ce tutoriel est placé sous [Licence Art Libre](http://artlibre.org/licence/lal/).